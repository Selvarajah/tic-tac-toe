/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */

package ch.privat.Model;

import java.util.List;
import java.util.ArrayList;
import javafx.scene.image.Image;

/**
 *
 * @author Jekathmenan
 */
public class Player 
{
    private int score = 0;
    private final Image playerImage;
    private boolean one = false;
    private boolean two = false;
    private boolean three = false;
    private boolean four = false;
    private boolean five = false;
    private boolean six = false;
    private boolean seven = false;
    private boolean eight = false;
    private boolean nine = false;

    /**
     * 
     * A constructor to set Players unique image
     * 
     * @param playerImage 
     * 
     */
    public Player(Image playerImage)
    {
        this.playerImage = playerImage;
    }

    public void addChosen(int field)
    {
        switch (field)
        {
            case 1:
                one = true;
                break;
            case 2:
                two = true;
                break;
            case 3:
                three = true;
                break;
            case 4:
                four = true;
                break;
            case 5:
                five = true;
                break;
            case 6:
                six = true;
                break;
            case 7: 
                seven = true;
                break;
            case 8:
                eight = true;
                break;
            case 9:
                nine = true;
                break;
            default :
                break;
        }
    }
    
    public void addScore(int score)
    {
        this.score += score;
    }
    
    public void resetGame()
    {
        one = false;
        two = false;
        three = false;
        four = false;
        five = false;
        six = false;
        seven = false;
        eight = false;
        nine = false;
    }
    
    public boolean checkWin()
    {
        if(one && two && three || four && five && six || seven && eight && nine || one && five && nine || three && five && seven || one && four && seven || two && five && eight || three && six && nine)
        {
            resetGame();
            return true;
        }
        else return false;
        
    }
    
    public int getScore()
    {
        return score;
    }

    public Image getPlayerImage()
    {
        return playerImage;
    }
}
