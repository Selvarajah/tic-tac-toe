/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */
package ch.privat.Model;

import ch.privat.tictactoe;
import com.sun.prism.paint.Color;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javax.swing.JOptionPane;

/**
 *
 * @author Jekathmenan
 */
public class Model
{
    protected final PropertyChangeSupport changes = new PropertyChangeSupport(this);
    private final tictactoe mainApp;
    
    private final Player player1;
    private final Player player2;
    private Player currentPlayer;
    
    /**
     * 
     * A constructor to set mainApp
     * 
     * @param mainApp 
     * @throws java.io.FileNotFoundException 
     * 
     */
    public Model(tictactoe mainApp) throws FileNotFoundException
    {
        this.player2 = new Player(new Image(new FileInputStream("C:/Users/jekat/Desktop/player1.jpg")));
        this.player1 = new Player(new Image(new FileInputStream("C:/Users/jekat/Desktop/Player2.jpg")));
        this.mainApp = mainApp;
        currentPlayer = player1;
    }
    
    /**
     * 
     * Exits the program -- Doesn't ask for confirmation
     * 
     */
    public void exitAction()
    {
        System.exit(0);
    }
    
    /**
     * 
     * A method to handle the chosen Field action
     * 
     * @param imageView
     * @param field 
     * @param btn 
     * 
     */
    public void tictactoeAction(ImageView imageView, int field, Button btn)
    {
        btn.setDisable(true);
        // Sets the correct image to ImageView(chosen gamefield)
        imageView.setImage(currentPlayer.getPlayerImage());
        
        
        //marks field as chosen
        currentPlayer.addChosen(field);
        
        // checks if current player has won
        if(currentPlayer.checkWin())
        {
            if(currentPlayer.equals(player1))
            {
                // Adds point to currentPlayer
                currentPlayer.addScore(200);
                
                
                JOptionPane.showMessageDialog
                (
                    null,                          
                    "Player 1 wins.",
                    "Won",
                    JOptionPane.WARNING_MESSAGE
                );
                
                currentPlayer = player1;
            }
            else
            {
                currentPlayer.addScore(200);
                
                JOptionPane.showMessageDialog
                (
                    null,                          
                    "Player 2 wins.",
                    "Won",
                    JOptionPane.WARNING_MESSAGE
                );
                
                currentPlayer = player1;
            }
        }
        
        // switches player
        if(currentPlayer.equals(player1))
        {
            currentPlayer = player2;
            imageView.setStyle("-fx-background-color: RED");
        }
        else
        {
            currentPlayer = player1;
            imageView.setStyle("-fx-background-color: RED");
        }
        
        firePropertyChanges();
    }
    
    public void firePropertyChanges()
    {
        changes.firePropertyChange("Score player1", null, player1.getScore());
        
        changes.firePropertyChange("Score player2", null, player2.getScore());
    }
    
    /**
     * 
     * A method to change View
     * 
     * @param title
     * @param url
     * @param listener
     * @throws IOException 
     * 
     * Note: This method also removes Listener from changes
     * 
     */
    public void changeView(String title, String url, PropertyChangeListener listener) throws IOException
    {
        removePropertyChangeListener(listener);
        mainApp.changeView(title, url);
    }
    
    /**
     * 
     * A method to add a given PropertyChangeListener 'l' to changes
     * 
     * @param l 
     * 
     */
    public void addPropertyChangeListener(PropertyChangeListener l)
    {
        changes.addPropertyChangeListener(l);
    }
    
    /**
     * 
     * A method to remove the given PropertyChangeListener 'listener' from changes
     * 
     * @param listener 
     * 
     */
    public void removePropertyChangeListener(PropertyChangeListener listener)
    {
        changes.removePropertyChangeListener(listener);   
    }
    
    public boolean hasWon()
    {
        return currentPlayer.checkWin();
    }
}
