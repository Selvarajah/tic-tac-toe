/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.privat.view;

import ch.privat.viewModel.GameViewModel;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author Jekathmenan
 */
public class GameView implements Initializable
{
    private GameViewModel viewModel;
    
    @FXML
    private Label lblPointPlayer1;
    @FXML
    private Label lblPointPlayer2;
    
    @FXML
    private Button btn1;
    @FXML
    private Button btn2;
    @FXML
    private Button btn3;
    @FXML
    private Button btn4;
    @FXML
    private Button btn5;
    @FXML
    private Button btn6;
    @FXML
    private Button btn7;
    @FXML
    private Button btn8;
    @FXML
    private Button btn9;
    @FXML
    private ImageView image1;
    @FXML
    private ImageView image2;
    @FXML
    private ImageView image3;
    @FXML
    private ImageView image4;
    @FXML
    private ImageView image5;
    @FXML
    private ImageView image6;
    @FXML
    private ImageView image7;
    @FXML
    private ImageView image8;
    @FXML
    private ImageView image9;

    /**
     * 
     * Initializes the controller class.
     * @param url
     * @param rb
     * 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        // TODO
    }    
    
    public void bind()
    {
        lblPointPlayer1.textProperty().bind(viewModel.getScorePlayer1());
        lblPointPlayer2.textProperty().bind(viewModel.getScorePlayer2());
        
        if(viewModel.checkWon())
        {
            resetBoard();
        }
    }
    
    public void resetBoard()
    {
        btn1.setDisable(false);
        btn2.setDisable(false);
        btn3.setDisable(false);
        btn4.setDisable(false);
        btn5.setDisable(false);
        btn6.setDisable(false);
        btn7.setDisable(false);
        btn8.setDisable(false);
        btn9.setDisable(false);
    }

    @FXML
    private void backAction(ActionEvent event)
    {
        try
        {
            viewModel.backAction();
        } 
        catch (IOException ex)
        {
            System.out.println("An error occured!");
        }
    }

    @FXML
    private void exitAction(ActionEvent event)
    {
        viewModel.exitAction();
    }

    @FXML
    private void btn1Action(ActionEvent event)
    {
        viewModel.tictactoeAction(image1, 1, btn1);
    }

    @FXML
    private void btn2Action(ActionEvent event)
    {
        viewModel.tictactoeAction(image2, 2, btn2);
    }

    @FXML
    private void btn3Action(ActionEvent event)
    {
        viewModel.tictactoeAction(image3, 3, btn3);
    }

    @FXML
    private void btn4Action(ActionEvent event)
    {
        viewModel.tictactoeAction(image4, 4, btn4);
    }

    @FXML
    private void btn5Action(ActionEvent event)
    {
        viewModel.tictactoeAction(image5, 5, btn5);
    }

    @FXML
    private void btn6Action(ActionEvent event)
    {
        viewModel.tictactoeAction(image6, 6, btn6);
    }

    @FXML
    private void btn7Action(ActionEvent event)
    {
        viewModel.tictactoeAction(image7, 7, btn7);
    }

    @FXML
    private void btn8Action(ActionEvent event)
    {
        viewModel.tictactoeAction(image8, 8, btn8);
    }

    @FXML
    private void btn9Action(ActionEvent event)
    {
        viewModel.tictactoeAction(image9, 9, btn9);
    }
    
    public void setViewModel(GameViewModel viewModel)
    {
        this.viewModel = viewModel;
    }
}
