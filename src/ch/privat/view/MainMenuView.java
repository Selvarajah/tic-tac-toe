/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.privat.view;

import ch.privat.viewModel.MainMenuViewModel;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author Jekathmenan
 */
public class MainMenuView implements Initializable
{
    private MainMenuViewModel viewModel;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        // TODO
    }    
    
    /**
     * 
     * A method to bind all the javafx-components with the values
     * 
     */
    public void bind()
    {
        
    }
    
    /**
     * 
     * A method to handle the play Action
     * 
     * Note: Can throw an exception
     * 
     * @param event 
     * 
     */
    @FXML
    private void playAction(ActionEvent event)
    {
        try
        {
            viewModel.playAction();
        } 
        catch (IOException ex)
        {
            System.out.println("An Error Occured");
        }
    }
    
    /**
     * 
     * A method to handle the exitAction
     * 
     * Note: Calls viewModel's exitAction
     * 
     * @param event 
     * 
     */
    @FXML
    private void exitAction(ActionEvent event)
    {
        viewModel.exitAction();
    }

    /**
     * 
     * A setter for viewModel
     * 
     * @param viewModel 
     * 
     */
    public void setViewModel(MainMenuViewModel viewModel)
    {
        this.viewModel = viewModel;
    }
}
