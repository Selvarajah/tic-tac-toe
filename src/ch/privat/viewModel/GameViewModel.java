/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */

package ch.privat.viewModel;

import ch.privat.Model.Model;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

/**
 *
 * @author Jekathmenan
 */
public class GameViewModel implements PropertyChangeListener
{
    private final Model model;
    
    private StringProperty scorePlayer1 = new SimpleStringProperty();
    private StringProperty scorePlayer2 = new SimpleStringProperty();
    /**
     * 
     * A constructor to initialize model
     * 
     * @param model 
     * 
     */
    public GameViewModel(Model model)
    {
        this.model = model;
    }
    
    /**
     * 
     * A method to go back to MainMenu
     * 
     * Note: Calls model's changeView with the asked parameters
     * 
     * @throws IOException 
     */
    public void backAction() throws IOException
    {
        model.changeView("Tic Tac Toe - Main Menu", "view/MainMenuView.fxml", this);
    }
    
    /**
     * 
     * A method to handle the exit action
     * 
     */
    public void exitAction()
    {
        model.exitAction();
    }
    
    public boolean checkWon()
    {
        return model.hasWon();
    }
    
    public void tictactoeAction(ImageView imageView, int field, Button btn)
    {
        model.tictactoeAction(imageView, field, btn);
    }
    
    /**
     * 
     * An implemented method from Interface PropertyChangeListener
     * 
     * Note: This method handles the propertyChange action
     * 
     * @param evt 
     * 
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt)
    {
        switch(evt.getPropertyName())
        {
            case "Score player1":
                scorePlayer1.set(evt.getNewValue().toString());
                break;
            case "Score player2":
                scorePlayer2.set(evt.getNewValue().toString());
                break;
        }
    }

    public StringProperty getScorePlayer1()
    {
        return scorePlayer1;
    }

    public StringProperty getScorePlayer2()
    {
        return scorePlayer2;
    }
}
