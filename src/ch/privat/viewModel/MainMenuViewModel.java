/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */
package ch.privat.viewModel;

import ch.privat.Model.Model;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;

/**
 *
 * @author Jekathmenan
 */
public class MainMenuViewModel implements PropertyChangeListener
{
    private Model model;

    /**
     * 
     * A constructor to initialize a model
     * 
     * @param model 
     * 
     */
    public MainMenuViewModel(Model model)
    {
        this.model = model;
    }
    
    /**
     * 
     * A method to handle the exit action
     * 
     */
    public void exitAction()
    {
        model.exitAction();
    }
    
    /**
     * 
     * A method to handle the play action
     * 
     * Note: This method simply calls models changeView with the asked parameters
     * 
     * @throws IOException 
     * 
     */
    public void playAction() throws IOException
    {
        model.changeView("Tic Tac Toe - Game", "view/GameView.fxml", this);
        
    }

    /**
     * 
     * An implemented method from Interface PropertyChangeListener
     * 
     * Note: This method handles the propertyChange action
     * 
     * @param evt 
     * 
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt)
    {
        
    }
}
