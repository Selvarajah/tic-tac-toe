/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.privat;

import ch.privat.Model.Model;
import ch.privat.view.GameView;
import ch.privat.view.MainMenuView;
import ch.privat.viewModel.GameViewModel;
import ch.privat.viewModel.MainMenuViewModel;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Jekathmenan
 */
public class tictactoe extends Application
{
    private Stage stage;
    private Model model;
    
    /**
     * 
     * A method to start the program
     * 
     * @param primaryStage
     * @throws IOException 
     * 
     * Note: This method is called in the beginning of the program by the main method
     * In this method the First view is shown.
     * 
     */
    @Override
    public void start(Stage primaryStage) throws IOException
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/MainMenuView.fxml"));
        Parent root = loader.load();
        
        MainMenuView view = loader.getController();
        model = new Model(this);
        final MainMenuViewModel viewModel = new MainMenuViewModel(model);
        
        
        view.setViewModel(viewModel);
        model.addPropertyChangeListener(viewModel);
        view.bind();
        
        stage = primaryStage;
        
        Scene scene = new Scene(root);
        
        primaryStage.setTitle("Tic Tac Toe - Main Menu");
        primaryStage.setResizable(false);
        
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    /**
     * 
     * A method to change the view
     * 
     * @param title
     * @param url
     * @throws IOException 
     * 
     */
    public void changeView(String title, String url) throws IOException
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(url));
        Parent root = loader.load();
        Scene scene;

        // setting up the views
        switch (title)
        {
            case "Tic Tac Toe - Game":
                GameView gameView = loader.getController();
                final GameViewModel gameVM = new GameViewModel(model);
                
                gameView.setViewModel(gameVM);
                model.addPropertyChangeListener(gameVM);
                gameView.bind();
                break;
            case "Tic Tac Toe - Main Menu":
                MainMenuView mmView = loader.getController();
                final MainMenuViewModel mmViewModel = new MainMenuViewModel(model);
                
                mmView.setViewModel(mmViewModel);
                model.addPropertyChangeListener(mmViewModel);
                mmView.bind();
                break;
        }

        // root with the view is bound to the scene
        scene = new Scene(root);

        //setting title and scene to stage
        stage.setTitle(title);
        stage.setScene(scene);
        
        //showing stage
        stage.show();
    }

    /**
     * 
     * Main Method to start the application
     * 
     * @param args the command line arguments
     * 
     */
    public static void main(String[] args)
    {
        launch(args);
    }
}
